# productapi - Avenue Code
productapi is a hiring process coding test proposed by Avenue Code
=== 
### Pre-requisites:
* Java 7;

### Install the Application:

Clone this repository:
```sh
git clone git@bitbucket.org:pafranca/productapi_avenuecode.git
```

Run the application tests:
```sh
mvn test
```

Run the application:
```sh
mvn spring-boot:run
```

## REST URLS FOR READ/GET ##

* http://localhost:8080/product
* http://localhost:8080/product/{id}
* http://localhost:8080/product/simple/{id}
* http://localhost:8080/product/{id}/images
* http://localhost:8080/product/{id}/child

## REST URLS POST/CREATE ##

to create a new product 
post to http://localhost:8080/product

```sh
curl -i -X POST -H "Content-Type: application/json" -d '{"id":1,"name":"Harley Davidson","description":"Harley Davidson Fatboy 103 cubic Inches","images":null,"parent":null}' http://localhost:8080/product
```
## Comments
* This solution is based upon Spring-Boot framework that facilitates app developing from the scratch. 
* The entities were built taking in consideration the model provided by avenue code.
* Regarding the product images the idea is to use Base64 in order to persist the images on database, that's why I chose LOB data type.

## References
Title: Spring REST
Authors: Balaji Varanasi, Sudha Belida
link: http://www.amazon.com/Spring-REST-Balaji-Varanasi/dp/1484208242

Title: Unit Testing of Spring MVC Controllers: REST API
Author: Petri Kainulainen
link: http://www.petrikainulainen.net/programming/spring-framework/unit-testing-of-spring-mvc-controllers-rest-api/

Title: Spring Boot Reference Guide
Authors: Phillip Webb, Dave Syer, Josh Long, Stéphane Nicoll, Rob Winch, Andy Wilkinson, Marcel Overdijk, Christian Dupuis, Sébastien Deleuze, Michael Simons
link: http://docs.spring.io/autorepo/docs/spring-boot/current/reference/htmlsingle/

