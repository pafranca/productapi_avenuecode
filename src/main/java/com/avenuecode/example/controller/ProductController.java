package com.avenuecode.example.controller;

import java.net.URI;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.avenuecode.example.exception.ResourceNotFoundException;
import com.avenuecode.example.model.Image;
import com.avenuecode.example.model.Product;
import com.avenuecode.example.model.dto.ProductDTO;
import com.avenuecode.example.service.ProductService;

@Controller("productController")
@RequestMapping(value = "/product")
public class ProductController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	private ProductService service;
	
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Void> create(@Valid @RequestBody ProductDTO dto) {
        LOGGER.info("Adding new product with information: {}", dto);
        Product added = service.add(dto);
        LOGGER.info("Added product with information: {}", added);

        //Set the location header for newly created resource
        HttpHeaders responseHeaders = new HttpHeaders();
        URI newTrainingUri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/id")
                .buildAndExpand(added.getId())
                .toUri();
        responseHeaders.setLocation(newTrainingUri);

        return new ResponseEntity<Void>(null, responseHeaders, HttpStatus.CREATED);
    }
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Iterable<Product>> findAll() {
		LOGGER.info("Searching all products information");
		Iterable<Product> products = service.findAll();
		return new ResponseEntity<Iterable<Product>>(products, HttpStatus.OK);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ResponseEntity<Product> update(@PathVariable String id, @RequestBody ProductDTO dto) throws ResourceNotFoundException {
        LOGGER.info("Updating product with information: {}", dto);
        Product updated = service.update(dto);
        LOGGER.info("Updated product with information: {}", updated);
        return new ResponseEntity<Product>(updated, HttpStatus.OK);
    }
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> remove(@PathVariable String id) throws ResourceNotFoundException {
        LOGGER.info("Deleting product with id: {}", id);
        service.remove(Long.getLong(id));
        LOGGER.info("Deleted product with id: {}", id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> get(@PathVariable("id") final Long id) throws ResourceNotFoundException {
        LOGGER.info("Getting product with id: " + id);
        Product product = service.findOne(id);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/simple/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> getSimple(@PathVariable("id") final Long id) throws ResourceNotFoundException {
        LOGGER.info("Getting product with id: " + id);
        Product product = service.findOneSimple(id);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/{id}/images", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Image>> getProductImages(@PathVariable("id") final Long id) throws ResourceNotFoundException {
        LOGGER.info("Getting product images with id: " + id);
        Iterable<Image> images = service.findImages(id);
        return new ResponseEntity<Iterable<Image>>(images, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/{id}/child",method = RequestMethod.GET)
	public ResponseEntity<Iterable<Product>> getProductsChild(@PathVariable("id") final Long id) {
		LOGGER.info("Searching all products child information");
		Iterable<Product> products = service.findChild(id);
		return new ResponseEntity<Iterable<Product>>(products, HttpStatus.OK);
	}
}