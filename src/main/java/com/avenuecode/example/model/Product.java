package com.avenuecode.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author Paulo França
 *
 */

@Entity
@Table(name = "products")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "product_id")
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "description")
	private String description;
	@OneToMany(targetEntity = Image.class, cascade = CascadeType.ALL)
	private List<Image> images;
	@ManyToOne
	@JoinColumn(name = "parent_id", referencedColumnName = "product_id")
	private Product parent;
	
	public Product() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Image> getImages() {
		return images;
	}

	public void addImages(Image image) {
		if (images == null) {
			images = new ArrayList<Image>();
		}
		images.add(image);
	}

	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}
	
	public void update(String name, String description, List<Image> images, Product parent) {
		this.name = name;
		this.description = description;
		this.images = images;
		this.parent = parent;
	}
	
	public static Builder getBuilder(String name, String description) {
        return new Builder(name, description);
    }
	
	public static class Builder {
		private Product built;
		
		public Builder(String name, String description) {
			built = new Product();
			built.name = name;
			built.description = description;
		}
		
		public Builder images(List<Image> images) {
			for (Image image : images) {
				built.addImages(image);
			}
			return this;
		}
		
		public Builder image(Image image) {
			built.addImages(image);
			return this;
		}
		
		public Builder parent(Product parent) {
			built.parent = parent;
			return this;
		}
		
		public Product build() {
			return built;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((images == null) ? 0 : images.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (images == null) {
			if (other.images != null)
				return false;
		} else if (!images.equals(other.images))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", images=").append(images).append(", parent=")
				.append(parent).append("]");
		return builder.toString();
	}
}