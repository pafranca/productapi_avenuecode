package com.avenuecode.example.model.dto;

/**
 * 
 * @author Paulo França
 *
 */
public class ImageDTO {
	private Long id;
	private String name;
	private String type;
	private String content;

	public ImageDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImageDTO [id=").append(id).append(", name=").append(name)
				.append(", type=").append(type).append("]");
		return builder.toString();
	}
}