package com.avenuecode.example.model.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Paulo França
 *
 */
public class ProductDTO {
	private Long id;
	private String name;
	private String description;
	private List<ImageDTO> images;
	private ProductDTO parent;

	public ProductDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ImageDTO> getImages() {
		return images;
	}

	public void addImages(ImageDTO image) {
		if (images == null) {
			images = new ArrayList<ImageDTO>();
		}
		images.add(image);
	}

	public ProductDTO getParent() {
		return parent;
	}

	public void setParent(ProductDTO parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductDTO [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", images=").append(images).append(", parent=")
				.append(parent).append("]");
		return builder.toString();
	}
}