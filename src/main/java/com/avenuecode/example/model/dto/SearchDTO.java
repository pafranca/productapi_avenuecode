package com.avenuecode.example.model.dto;

/**
 * @author Paulo França
 */
public class SearchDTO {
	private int pageIndex;
	private int pageSize;
	private String searchTerm;

	public SearchDTO() {
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchDTO [pageIndex=").append(pageIndex)
				.append(", pageSize=").append(pageSize).append(", searchTerm=")
				.append(searchTerm).append("]");
		return builder.toString();
	}
}