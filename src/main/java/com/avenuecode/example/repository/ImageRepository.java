package com.avenuecode.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.avenuecode.example.model.Image;

public interface ImageRepository extends JpaRepository<Image, Long>,
		JpaSpecificationExecutor<Image> {
}
