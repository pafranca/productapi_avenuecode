package com.avenuecode.example.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.example.exception.ResourceNotFoundException;
import com.avenuecode.example.model.Image;
import com.avenuecode.example.model.Product;
import com.avenuecode.example.model.dto.ImageDTO;
import com.avenuecode.example.model.dto.ProductDTO;
import com.avenuecode.example.repository.ProductRepository;

/**
 * 
 * @author Paulo França
 * 
 */
@Service("productService")
public class ProductServiceImpl implements ProductService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	private ProductRepository repository;

	@Override
	public Product add(ProductDTO added) {
		LOGGER.info("Adding new product with information {}", added);
		Product built = buildProduct(added);
		return repository.save(built);
	}

	@Override
	public Product update(ProductDTO updated) throws ResourceNotFoundException {
		LOGGER.info("Updating product with information {}", updated);
		Product found = repository.findOne(updated.getId());
		if (found == null) {
            LOGGER.info("No product found with id {}", updated.getId());
            throw new ResourceNotFoundException("No product found with id: " + updated.getId());
        }
		
        found.update(updated.getName(), updated.getDescription(), buildImages(updated.getImages()), buildProduct(updated.getParent()));
        repository.save(found);
        return found;
	}

	@Override
	public boolean remove(Long id) throws ResourceNotFoundException {
		LOGGER.info("Deleting product by id {}", id);
        Product product = this.findOne(id);
        repository.delete(product);
        LOGGER.info("Deleted product {}", product);
        return true;
	}

	@Override
	public List<Product> findAllSimple() {
		LOGGER.info("Finding all products without relationship");
		List<Product> products = findAll();
		removeAllRelationship(products);
		return products;
	}

	@Override
	public List<Product> findAll() {
		LOGGER.info("Finding all products");
        return repository.findAll();
	}

	@Override
	public Product findOneSimple(Long id) throws ResourceNotFoundException {
		Product found = findOne(id);
		removeRelationship(found);
        return found;
	}

	@Override
	public Product findOne(Long id) throws ResourceNotFoundException {
		LOGGER.info("Finding product by id {}", id);
        Product found = repository.findOne(id);
        if (found == null) {
            LOGGER.info("No product found with id {}", id);
            throw new ResourceNotFoundException("No product found with id: " + id);
        }
        LOGGER.info("Found product: " + found);
        return found;
	}

	@Override
	public List<Product> findChild(Long id) throws ResourceNotFoundException {
		LOGGER.info("Finding child products by id {}", id);
        List<Product> found = repository.findChildByProductId(id);
        if (found == null) {
            LOGGER.info("No products found with id {}", id);
            throw new ResourceNotFoundException("No child found with id: " + id);
        }
        LOGGER.info("Found products: " + found);
        return found;
	}

	@Override
	public List<Image> findImages(Long id) throws ResourceNotFoundException {
		Product product = findOne(id);
		return product.getImages();
	}

	private Product buildProduct(ProductDTO added) {
		return added != null ? Product.getBuilder(added.getName(), added.getDescription())
				.images(buildImages(added.getImages()))
				.parent(added.getParent() != null ? buildProduct(added.getParent()) : null)
				.build() : null;
	}

	private List<Image> buildImages(List<ImageDTO> imagesDTO) {
		List<Image> images = new ArrayList<Image>();
		if (imagesDTO != null) {
			for (ImageDTO dto : imagesDTO) {
				images.add(Image.getBuilder(dto.getName(), dto.getType())
						.content(dto.getContent()).build());
			}
		}
		return images;
	}
	
	private void removeAllRelationship(List<Product> products) {
		for (Product product : products) {
			removeRelationship(product);
		}		
	}

	private void removeRelationship(Product product) {
		product.update(product.getName(), product.getDescription(), null, null);
	}
}
