package com.avenuecode.example.service;

import java.util.List;

import com.avenuecode.example.exception.ResourceNotFoundException;
import com.avenuecode.example.model.Image;
import com.avenuecode.example.model.Product;
import com.avenuecode.example.model.dto.ProductDTO;

public interface ProductService {
	/**
     * Adds a new product.
     * @param added The information of the added product.
     * @return  The added product.
     */
	public Product add(ProductDTO added);
	
	/**
     * Updates the information of a product.
     * @param updated   The new information of a product.
     * @return  The updated product.
     * @throws ResourceNotFoundException   if no product is found with the provided id.
     */
	public Product update(ProductDTO updated) throws ResourceNotFoundException;
	
	/**
     * Deletes a product.
     * @param id    The id of the deleted product.
     * @return  The deleted product.
     * @throws ResourceNotFoundException   if a product is not found with the given id.
     */
	public boolean remove(Long id) throws ResourceNotFoundException;
	
	
	/**
     * Finds all products excluding relationship
     * @return  A list of products.
     */
	public List<Product> findAllSimple();
	
	/**
     * Finds all products
     * @return  A list of products.
     */
	public List<Product> findAll();
	
	/**
     * Finds a product without its relationship.
     * @param id    The id of the wanted product.
     * @return  The found product.
     * @throws ResourceNotFoundException    if no product is found with the given id.
     */
	public Product findOneSimple(Long id) throws ResourceNotFoundException;
	
	/**
     * Finds a product.
     * @param id    The id of the wanted product.
     * @return  The found product.
     * @throws ResourceNotFoundException    if no product is found with the given id.
     */
	public Product findOne(Long id) throws ResourceNotFoundException;
	
	/**
     * Finds its all relationship
     * @param id    The id of the wanted product.
     * @return  The relationship of found product.
     * @throws ResourceNotFoundException    if no product is found with the given id.
     */
	public List<Product> findChild(Long id) throws ResourceNotFoundException;
	
	/**
     * Finds all images from the product.
     * @param id    The id of the wanted product.
     * @return  A list of images.
     * @throws ResourceNotFoundException    if no product is found with the given id.
     */
	public List<Image> findImages(Long id) throws ResourceNotFoundException;
	
}
