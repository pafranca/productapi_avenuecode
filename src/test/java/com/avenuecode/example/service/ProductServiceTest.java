package com.avenuecode.example.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.avenuecode.example.exception.ResourceNotFoundException;
import com.avenuecode.example.model.Product;

@RunWith(SpringJUnit4ClassRunner.class)
public class ProductServiceTest {
	@InjectMocks
	private ProductService productService;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testAddProduct() {
		Product motorcycle = Product.getBuilder("Harley Davidson", "Harley Davidson Softail Slim 103 cubic Inches")
				.build();
		//repository.save(motorcycle);
	}
	
	@Test
	public void testUpdateProduct() throws ResourceNotFoundException {
		
	}
	
	@Test
	public void removeProduct() throws ResourceNotFoundException {
		
	}
	
	@Test
	public void findAllSimpleProducts() {
		
	}
	
	
	@Test
	public void findAll() {
		
	}
	
	@Test
	public void findOneSimple() throws ResourceNotFoundException {
		
	}
	
	@Test
	public void findOne() throws ResourceNotFoundException {
		
	}
	
	@Test
	public void findChild() throws ResourceNotFoundException {
		
	}
	
	@Test
	public void findImages() throws ResourceNotFoundException {
		
	}
	
}
