package com.avenuecode.example.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.avenuecode.Application;
import com.avenuecode.example.model.Product;
import com.avenuecode.example.service.ProductService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class ProductControllerTest {
	private MockMvc mockMvc;
	
	@Mock
	private ProductService productServiceMock;
	
	@Autowired
	private WebApplicationContext context;
	
	@Before
	public void setup() {
		Mockito.reset(productServiceMock);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
	
	@Test
	public void testSaveProduct() throws Exception {
		mockMvc.perform(post("/product/")
                .content(TestUtil.convertObjectToJsonBytes(getDyna()))
                .contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());
    }
	
	@Test
	public void testGetAllProducts() throws Exception {
		mockMvc.perform(get("/product/"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].id", is(1)))
            .andExpect(jsonPath("$[0].description", is("Harley Davidson Softail Slim 103 cubic Inches")))
            .andExpect(jsonPath("$[0].name", is("Softail Slim")))
            .andExpect(jsonPath("$[1].id", is(2)))
            .andExpect(jsonPath("$[1].description", is("Vance and Hines exhaust 2 into 1")))
            .andExpect(jsonPath("$[1].name", is("Vance And Hines")));
		
	}
	
	@Test
	public void testInvalidProduct() throws Exception {
		mockMvc.perform(get("/product/{id}", -1L))
		.andExpect(status().isNotFound())
		.andExpect(jsonPath("$.status").value(404))
		.andExpect(jsonPath("$.detail", is("No product found with id: -1")));
	}
	
	@Test
	public void testUpdatingProduct() throws Exception {
		Product dyna = getDyna();
		dyna.setId(3L);
		dyna.setDescription("Harley Davidson Dyna Super Glide Custom 103 cubic inches");
		
		mockMvc.perform(put("/product/{id}", 3L)
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dyna))
        )
        	.andExpect(status().isOk())
            .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id", is(3)))
            .andExpect(jsonPath("$.description", is("Harley Davidson Dyna Super Glide Custom 103 cubic inches")));
	}
	
	private Product getDyna() {
		return Product.getBuilder("Dyna Super Glide Custom", 
				"Harley Davidson Dyna Super Glide Custom 96 cubic inches")
				.build();
	}
}